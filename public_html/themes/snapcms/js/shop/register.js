$(document).ready(function(){
    var $locSel = $("select#BoxomaticUser_location_id");
    var $daySel = $("select#BoxomaticUser_delivery_day");
    $locSel.change(function() {
        
        var $optGroup = $locSel.find(":selected").parent();
        var pos = $locSel.find("optgroup").index($optGroup);
        if(pos === 1) { //delivery location
            $('div#delivery-address').removeClass('hidden');
        } else {
             $('div#delivery-address').addClass('hidden');
        }
        
        var val = $(this).val();
        
        $daySel.html('');
        if(val !== '') {
            var days = locationDays[val];
            $.each(days, function(key, value){
               $daySel.append("<option value="+key+">"+value+"</option>");
            });
        } else {
            $daySel.html('<option>- Select Location -</option>');
        }
        
    });
});