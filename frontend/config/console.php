<?php

define('SNAP_FRONTEND_URL', '');
define('SNAP_BACKEND_URL', '/admin');

// uncomment the following to define a path alias
Yii::setPathOfAlias('snapcms', __DIR__ . '/../../vendor/snapfrozen/snapcms-legacy');
Yii::setPathOfAlias('frontend', __DIR__.'/../../frontend');
Yii::setPathOfAlias('web', __DIR__.'/../../public_html');
Yii::setPathOfAlias('vendor', __DIR__.'/../../vendor');

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'     => 'My Console Application',
    // preloading 'log' component
    'preload' => array('log'),
    'import' => array(
        'snapcms.models.*',
        'snapcms.components.*',
        'frontend.models.*',
        'frontend.components.*',
    ),
    // application components
    'components' => array(
        'db'   => require(__DIR__ . '/../../db.php'),
        // login web user as admin
        'user' => array(
            'class'      => 'SnapConsoleUser',
            'primaryKey' => 1
        ),
        'authManager' => array(
            'class' => 'SnapAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('Anonymous'),
        ),
        'log'  => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
);
