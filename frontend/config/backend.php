<?php

define('SNAP_FRONTEND_URL', '');
define('SNAP_BACKEND_URL', '/admin');

Yii::setPathOfAlias('snapcms', __DIR__ . '/../../vendor/snapfrozen/snapcms-legacy');
Yii::setPathOfAlias('frontend', __DIR__ . '/../../frontend');
Yii::setPathOfAlias('web', __DIR__ . '/../../public_html');
Yii::setPathOfAlias('vendor', __DIR__ . '/../../vendor');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '/../../vendor/snapfrozen/snapcms-legacy',
    'name'     => 'Boxomatic',
    'id'       => 'snapcms',
    // preloading 'log' component
    'preload' => array('log'),
    'theme'    => 'admin',
    // autoloading model and component classes
    'import' => array(
        //'frontend.components.*',
        'snapcms.models.*',
        'snapcms.components.*',
        'boxomatic.models.*',
        'boxomatic.components.*',
        'bootstrap.components.*',
        'bootstrap.helpers.*',
        'bootstrap.behaviors.*',
        'bootstrap.widgets.*',
    ),
    'aliases' => array(
        'bootstrap' => 'vendor.drmabuse.yii-bootstrap-3-module',
        'boxomatic' => 'vendor.snapfrozen.boxomatic',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        /*
        'gii' => array(
            'class'          => 'system.gii.GiiModule',
            'password'       => 'francis',
            'generatorPaths' => array(
                'bootstrap.gii',
                'application.gii',
            ),
            //'viewPath'=>'application.modules.snapcms.boats.views',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'      => array('127.0.0.1', '::1'),
        ),
         */
        'boxomatic' => array(
            'class' => 'boxomatic.SnapCMSBoxomaticModule'
        )
    ),
    // application components
    'components' => array(
        'db' => require(__DIR__.'/../../db.php'),
        'mail' => array(
            'class' => 'boxomatic.extensions.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'frontend.views.mail',
            //'logging' => true,
            //'dryRun' => false
        ),
        'themeManager' => array(
            'basePath' => '../themes/',
            'baseUrl'  => '../themes/',
        ),
        'bsHtml' => array(
            'class' => 'bootstrap.components.BsHtml'
        ),
        'authManager' => array(
            'class'        => 'SnapAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('Anonymous'),
        ),
        'user'    => array(
            // enable cookie-based authentication
            'class' => 'snapcms.components.SnapWebUser',
        ),
        'session' => array(
            //'savePath' => '/temp',
            'cookieMode'   => 'allow',
            'cookieParams' => array(
                'path'     => '/',
                'httpOnly' => true,
            //'domain' => 'snapcms.local',
            ),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'   => array(
            'class'          => 'snapcms.components.SnapUrlManager',
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => array(
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ),
        ),
        'cache'        => array(
            'class' => 'system.caching.CFileCache',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'        => 'CDbLogRoute',
                    'connectionID' => 'db',
                    'levels'       => 'error, warning, info',
                    'logTableName' => '{{log}}'
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              'categories'=>'system.db.CDbCommand',
              ),
             */
            ),
        ),
    ),
);
