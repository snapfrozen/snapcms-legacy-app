<?php
return array(
    'default' => array(
        //'attribute'=>'content',     
        'height'     => '400px',
        'width'      => '100%',
        'toolbarSet' => array(
            array(
                'name'  => 'paragraph',
                'items' => array('NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Blockquote')
            ),
            array(
                'name'  => 'links',
                'items' => array('Link', 'Unlink', 'Anchor')
            ),
            array(
                'name'  => 'links',
                'items' => array('Image', 'Table', 'HorizontalRule')
            ),
            '/',
            array(
                'name'  => 'styles',
                'items' => array('Format')
            ),
            array(
                'name'  => 'basicstyles',
                'items' => array('Bold', 'Italic', 'Underline')
            ),
            array(
                'name'  => 'clipboard',
                'items' => array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo')
            ),
            array(
                'name'  => 'tools',
                'items' => array('Maximize')
            ),
        ),
        'ckeditor'   => Yii::getPathOfAlias('snapcms.external.CKEditor') . '/ckeditor.php',
        //'ckBasePath' => $baseUrl . '/lib/ckeditor/',
        //'css' => $baseUrl.'/css/index.css',
        'config'     => array(),
    ),
    'plain'   => array(
        //'attribute'=>'content',     
        'height'     => '150px',
        'width'      => '100%',
        'toolbarSet' => array(
            array(
                'name'  => 'basicstyles',
                'items' => array('Bold', 'Italic', 'Underline')
            ),
        ),
        'ckeditor'   => Yii::getPathOfAlias('snapcms.external.CKEditor') . '/ckeditor.php',
        //'ckBasePath' => $baseUrl . '/lib/ckeditor/',
        //'css' => $baseUrl.'/css/index.css',
        'config'     => array(),
    ),
);
