<?php
return array(
    'sizes' => array(
        'small'         => array('w' => 120, 'h' => 120, 'zc' => 1),
        'medium'        => array('w' => 475, 'h' => 295, 'zc' => 1),
        'full'          => array('w' => 656),
        'gallery_thumb' => array('h' => 170),
    ),
);
