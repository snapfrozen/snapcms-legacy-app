<?php

//Can we "calculate" this?
define('SNAP_FRONTEND_URL', '');
define('SNAP_BACKEND_URL', '/admin');

Yii::setPathOfAlias('snapcms', __DIR__ . '/../../vendor/snapfrozen/snapcms-legacy');
Yii::setPathOfAlias('frontend', __DIR__ . '/../../frontend');
Yii::setPathOfAlias('web', __DIR__ . '/../../public_html');
Yii::setPathOfAlias('vendor', __DIR__ . '/../../vendor');

return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'id'         => 'snapcms', //Don't change this, it will break the backend to frontend session.
    'name'       => 'SnapCMS',
    'theme'      => 'snapcms',
    // preloading 'log' component
    'preload'    => array('log'),
    // autoloading model and component classes
    'import'     => array(
        'frontend.models.*',
        'frontend.components.*',
        'snapcms.models.*',
        'snapcms.components.*',
        //Import these if you are using the bootstrap module
        'bootstrap.helpers.*',
        'bootstrap.behaviors.*'
    ),
    'aliases'    => array(
        //If you are using the bootstrap module,
        'bootstrap' => 'vendor.drmabuse.yii-bootstrap-3-module'
    ),
    'modules'    => array(
        'payPal' => array(
            'env'        => '', // 'sandbox' or '' for live 
            'account'    => array(
                'username'      => '',
                'password'      => '',
                'signature'     => '',
                'email'         => '',
                'identityToken' => '',
            ),
            'components' => array(
                'buttonManager' => array(
                    //'class'=>'payPal.components.PPDbButtonManager'
                    'class' => 'payPal.components.PPPhpButtonManager',
                ),
            ),
        ),
        // uncomment the following to enable the Gii tool
        'gii'    => array(
            'class'          => 'system.gii.GiiModule',
            'password'       => 'francis',
            'generatorPaths' => array(
                'bootstrap.gii',
                'application.gii',
            ),
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'      => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'cache'        => array(
            'class' => 'system.caching.CFileCache'
        ),
        'authManager'  => array(
            'class'        => 'SnapAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('Anonymous'),
        ),
        'user'         => array(
            // enable cookie-based authentication
            'class'          => 'snapcms.components.SnapWebUser',
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'   => array(
            'class' => 'snapcms.components.SnapUrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'        => 'CDbLogRoute',
                    'connectionID' => 'db',
                    'levels'       => 'error, warning, info',
                    'logTableName' => '{{log}}'
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'      => 'CWebLogRoute',
                    'categories' => 'system.db.CDbCommand',
                ),
                 */
            ),
        ),
    ),
    'params'     => array(
        'Pin' => array(
            'secret_key' => ''
        )
    )
);
