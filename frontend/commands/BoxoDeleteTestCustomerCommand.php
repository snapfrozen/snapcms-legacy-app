<?php
/**
 * @author <jackson@snapfrozen.com.au>
 * Date: 12/02/2015
 * Time: 10:59
 */

class BoxoDeleteTestCustomerCommand extends CConsoleCommand
{
    public function run($args)
    {
        $message = "WARNING: THIS COMMAND WILL DELETE ALL CUSTOMERS AND THEIR ORDERS".PHP_EOL;
        $message .= "Are you sure want to run this command?";
        if ($this->confirm($message))
        {
            $customers = User::model()->deleteAll(User::model()->tableSchema->primaryKey . '!=' . Yii::app()->user->id);
            echo "$customers customers have been deleted.".PHP_EOL;
        }
    }
}