<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Running out of orders</title>
</head>
<body style="margin:0;">
<p>Hi <?php echo $Customer->User->first_name ?>,</p>
<p>
    Don’t forget to order your box of wonderful fresh local goodness for next week	Please <a href="<?php echo $this->createAbsoluteUrl('UserBox/order',array('key'=>$User->auto_login_key)); ?>">click here</a> to add more orders.</p>

<p>--<br />
    Warm regards,<br />
    The Bello Food Box Team<br />
    Ph: 1300 780 850<br />
    Email: <a href="mailto:<?php echo SnapUtil::config('boxomatic/adminEmail') ?>"><?php echo SnapUtil::config('boxomatic/adminEmail') ?></a></p>

<p>The Bello Food Box encourages sustainable agricultural practices and supports growth of our local economy and the development of a resilient local community. We distribute local organic &amp; chemical free produce that is ethically grown, affordable, healthy and picked fresh to order every week. Accessible from Coffs Harbour, North to Woologoolga &amp; South to Macksville, The Bello Food Box is the largest distributor of 100% local produce on the Mid North Coast.</p> 
</body>
</html>
