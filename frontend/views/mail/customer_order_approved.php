<?php
/**
 * @var $Customer User
 * @var $UserBoxes UserBox
 * @var $Order Order
 * @var $this BoxItemController
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Your order has been approved.</title>
</head>
<body style="margin:0;">
<p>Hi <?php echo $Customer->first_name ?>,</p>

<p><strong>Your <?php echo Yii::app()->name ?> order for <?php echo $Order->DeliveryDate->date ?>, has been APPROVED.</strong></p>

<p>Your pickup/delivery location is <?php echo $Order->delivery_location ?>.</p>

<p><strong>Managing your <?php echo Yii::app()->name ?> account</strong></p>

<p>You can check your account status, add credit and view previous orders at: <a href="https://www.bellofoodbox.com.au/">https://www.bellofoodbox.com.au/</a>.</p>

<p>You are welcome to top up your account credit by making additional payments at anytime. Box payments are only deducted each week as your orders are processed.</p>

<p>When depositing money via bank transfer, please use your <?php echo Yii::app()->name ?> ID as your reference.<br />
    Your <?php echo Yii::app()->name ?> ID is: <strong><?php echo $Customer->bfb_id; ?></strong>.
</p>

<p><strong>Order Items</strong></p>

<table>
    <thead>
    <tr>
        <th>Item Name</th>
        <th>Quantity</th>
        <th>price</th>
    </tr>
    </thead>
    <tbody>
    <!-- UserBox item -->
    <?php foreach ($Order->UserBoxes as $UserBox): ?>
        <tr>
            <td><?php echo CHtml::value($UserBox, 'Box.name') ?></td>
            <td><?php echo CHtml::value($UserBox, 'quantity') ?></td>
            <td><?php echo SnapFormat::currency(CHtml::value($UserBox, 'price')) ?></td>
        </tr>
    <?php endforeach; ?>
    <!-- Extras Item -->
    <?php foreach ($Order->Extras as $Extra): ?>
        <tr>
            <td><?php echo CHtml::value($Extra, 'name') ?></td>
            <td><?php echo CHtml::value($Extra, 'quantity') ?></td>
            <td><?php echo SnapFormat::currency(CHtml::value($Extra, 'price')) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
            <td><?php echo SnapFormat::currency($Order->total) ?></td>
        </tr>
    </tfoot>
</table>

<p><strong>Pickup &amp; delivery</strong></p>

<p>Home delivery to Coffs Harbour &amp; Woolgoolga areas is every Monday between Midday - 4pm.</p>

<p>Delivery to Valla Beach &amp; Macksville areas is on Tuesday morning to the drop off points specified on our website.</p>

<p>Local pickup is from the rear of 5 Church Street, Bellingen, every Monday between 3:30pm-5:30pm. We appreciate if you make best efforts to pick up your box within this time period. However if you are unable to pick up your box during this time, please contact us on 1300 780 850 and we will discuss other arrangements for you to collect your box.</p>

<p>Thanks very much for your order - we hope you enjoy your box of wonderful fresh local goodness next week! </p>
<p>--<br />
    Warm regards,<br />
    The Bello Food Box Team<br />
    Ph: 1300 780 850<br />
    Email: <a href="mailto:<?php echo SnapUtil::config('boxomatic/adminEmail') ?>"><?php echo SnapUtil::config('boxomatic/adminEmail') ?></a></p>
    
<p>The Bello Food Box encourages sustainable agricultural practices and supports growth of our local economy and the development of a resilient local community. We distribute local organic &amp; chemical free produce that is ethically grown, affordable, healthy and picked fresh to order every week. Accessible from Coffs Harbour, North to Woologoolga &amp; South to Macksville, The Bello Food Box is the largest distributor of 100% local produce on the Mid North Coast.</p>
</body>
</html>
