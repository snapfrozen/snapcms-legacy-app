<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="margin:0;">
<p>Hi <?php echo $Customer->first_name ?>,</p>

<p>Unfortunately your <?php echo Yii::app()->name ?> extras order for
  pick up on <?php echo $Order->DeliveryDate->date ?> has been DECLINED, as you
  have insufficient <?php echo Yii::app()->name ?> credit.</p>

<p>Please contact us immediately if you feel this is an error, or if you have just forgotten to make payment &amp; would still like your order to be approved for next week, please make payment by 9am TODAY and email us your payment receipt and we can slide you through!</p>

<p>You can top up your balance directly using PayPal and our secure credit card facility at <a href="https://www.bellofoodbox.com.au/">https://www.bellofoodbox.com.au/</a>.</p>

<p>Thanks very much</p>

<p>--<br />
    Warm regards,<br />
    The Bello Food Box Team<br />
    Ph: 1300 780 850<br />
    Email: <a href="mailto:<?php echo SnapUtil::config('boxomatic/adminEmail') ?>"><?php echo SnapUtil::config('boxomatic/adminEmail') ?></a></p>
    
<p>The Bello Food Box encourages sustainable agricultural practices and supports growth of our local economy and the development of a resilient local community. We distribute local organic &amp; chemical free produce that is ethically grown, affordable, healthy and picked fresh to order every week. Accessible from Coffs Harbour, North to Woologoolga &amp; South to Macksville, The Bello Food Box is the largest distributor of 100% local produce on the Mid North Coast.</p> 
</body>
</html>

