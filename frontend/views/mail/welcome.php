<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Welcome to <?php echo Yii::app()->name ?></title>
</head>
<body style="margin:0;">
	<p>Hi <?php echo $User->first_name ?>,</p>

	<p>Thank you for joining The <?php echo Yii::app()->name ?>! We look forward to supplying you with lots of local, wonderfully fresh chemical free produce - from harvest to home.</p>

	<p>Your login details are:</p>
	<p>Username: <?php echo $User->email ?><br />Password: <?php echo $newPassword ?></p>

	<p>You can check your account status, add credit and view previous orders at: <a href="https://www.bellofoodbox.com.au/">https://www.bellofoodbox.com.au/</a>.</p>

	<p>Each time you make a payment, we will send confirmation email of receipt.</p>

	<p>For each week you have an order placed, you'll receive a confirmation email, if there is enough credit in your <?php echo Yii::app()->name ?> account.</p>
	
	<p>If you have placed an order and we do not receive your <strong>payment by midnight on Wednesday</strong> prior to the following Monday’s delivery, we will send you an email stating that your order has been declined due to insufficient credit. We do offer a &quot;last chance&quot; each week to finalise your order and payment for this offer must be received by no later than 9am Thursday morning.</p>

	<p>You can top up your <?php echo Yii::app()->name ?> account at anytime, using either PayPal, or credit card payment.</p>

<p>Thanks again for joining the family - we look forward to packing your box of fresh local goodness real soon!</p>

<p>--<br />
    Warm regards,<br />
    The Bello Food Box Team<br />
    Ph: 1300 780 850<br />
    Email: <a href="mailto:<?php echo SnapUtil::config('boxomatic/adminEmail') ?>"><?php echo SnapUtil::config('boxomatic/adminEmail') ?></a></p>
    
<p>The Bello Food Box encourages sustainable agricultural practices and supports growth of our local economy and the development of a resilient local community. We distribute local organic &amp; chemical free produce that is ethically grown, affordable, healthy and picked fresh to order every week. Accessible from Coffs Harbour, North to Woologoolga &amp; South to Macksville, The Bello Food Box is the largest distributor of 100% local produce on the Mid North Coast.</p>    
</body>
</html>
