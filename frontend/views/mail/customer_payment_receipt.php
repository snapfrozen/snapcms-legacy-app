<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Payment Confirmation</title>
</head>
<body style="margin:0;">
<p>Hi <?php echo $Customer->first_name ?>,</p>

<p>Thank you for your Payment to <?php echo Yii::app()->name ?>.</p>

<p><strong><?php echo Yii::app()->name ?> payment details</strong></p>

<p>Name: <?php echo $Customer->first_name ?> <?php echo $Customer->last_name ?><br />
    Date: <?php echo $UserPayment->payment_date ?><br />
    Amount: <?php echo $UserPayment->payment_value ?><br />
    Method: <?php echo $UserPayment->payment_type ?></p>

<p><strong>Your current balance is $<?php echo $Customer->balance ?></strong></p>

<p>This is NOT an order confirmation. Details confirming any box orders will be sent in a separate email detailing pick up location and date.</p>

<p><strong>Managing your <?php echo Yii::app()->name ?> account</strong></p>

<p>You can check your account status, add credit and view previous orders at: <a href="https://www.bellofoodbox.com.au/">https://www.bellofoodbox.com.au/</a>.</p>

<p>You are welcome to top up your account
    credit by making additional payments at anytime.
    Box payments are only deducted each week as
    your orders are processed.</p>

<p>Thanks very much for your payment - we hope you enjoy your box of wonderful fresh local goodness!</p>
<p>--<br />
    Warm regards,<br />
    The Bello Food Box Team<br />
    Ph: 1300 780 850<br />
    Email: <a href="mailto:<?php echo SnapUtil::config('boxomatic/adminEmail') ?>"><?php echo SnapUtil::config('boxomatic/adminEmail') ?></a></p>
    
<p>The Bello Food Box encourages sustainable agricultural practices and supports growth of our local economy and the development of a resilient local community. We distribute local organic &amp; chemical free produce that is ethically grown, affordable, healthy and picked fresh to order every week. Accessible from Coffs Harbour, North to Woologoolga &amp; South to Macksville, The Bello Food Box is the largest distributor of 100% local produce on the Mid North Coast.</p> 
</body>
</html>
