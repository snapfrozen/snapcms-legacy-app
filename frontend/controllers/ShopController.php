<?php

class ShopController extends BoxomaticController
{

    public $Content;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class'     => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'    => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('error', 'register', 'index', 'contact', 'login', 'captcha', 'paypalFailure', 'paypalSuccess'),
                'users'   => array('*'),
            ),
            array('allow',
                'actions' => array('shop', 'checkout', 'changeLocation', 'process', 'welcome', 'removeOrder', 'confirmOrder', 'revertChanges'),
                'roles'   => array('View Content'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**
     * Manages all models.
     */
    public function actionIndex($cat = null)
    {
        $BoxoCart = new BoxoCart;

        if (!$cat) {
            $cat = SnapUtil::config('boxomatic/supplier_product_feature_category');
        }
        
        $userId = Yii::app()->user->id;
        $User = BoxomaticUser::model()->findByPk($userId);

        $Category = Category::model()->findByPk($cat);

        $DeliveryDate = false;
        $dpProducts = false;
        $Location = $BoxoCart->Location;
        $products = array();
        if ($Location)
        {
            if (!$BoxoCart->DeliveryDate) {
                $DeliveryDate = $BoxoCart->getNextDeliveryDate();
            }
            else {
                $DeliveryDate = $BoxoCart->DeliveryDate;
            }
            
            if ($DeliveryDate)
            {
                $BoxoCart->setDelivery_date_id($DeliveryDate->id);
                $products = SupplierProduct::getAvailableItems($DeliveryDate->id, $cat);
            }
            $dpProducts = new CActiveDataProvider('SupplierProduct');
            $dpProducts->setData($products);
        }

        $LoginForm = new LoginForm;

        // collect user input data
        if (isset($_POST['LoginForm']))
        {
            $LoginForm->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($LoginForm->validate() && $LoginForm->login())
            {
                $BoxoCart = new BoxoCart;
                $BoxoCart->populateCart();
                //$this->redirect(Yii::app()->user->returnUrl);
                $this->refresh();
            }
        }

        $this->render('index', array(
            'dpProducts' => $dpProducts,
            'DeliveryDate' => $DeliveryDate,
            'Category' => $Category,
            'Customer' => $User,
            'curCat' => $cat,
            'BoxoCart' => $BoxoCart,
            'Location' => $Location,
            'LoginForm' => $LoginForm,
        ));
    }

    public function actionConfirmOrder()
    {
        $BoxoCart = new BoxoCart;
        $minDays = SnapUtil::config('boxomatic/minimumAdvancePayment');
        $nextTotal = $BoxoCart->getNextTotal($minDays);

        if ($nextTotal != 0)
        {
            Yii::app()->user->setFlash('danger', 'You must add at least <strong>' . SnapFormat::currency($nextTotal) . '</strong> to your account to do this.');
        }
        else
        {
            $BoxoCart = new BoxoCart;
            $BoxoCart->confirmOrder();
            Yii::app()->user->setFlash('success', 'Your order has been updated.');
        }
        $this->redirect(array('/shop/checkout'));
    }

    public function actionChangeLocation()
    {
        $user = Yii::app()->user;
        if ($user->isGuest)
        {
            $BoxoCart = new BoxoCart;
            $BoxoCart->setLocation_id(null);
            $this->redirect(array('/shop/index'));
        }
        else
        {
            $this->redirect(array('/user/update', 'id' => $user->id));
        }
    }

    public function actionRemoveOrder($id)
    {
        $BoxoCart = new BoxoCart;
        $BoxoCart->removeOrder($id);
        Yii::app()->user->setFlash('success', 'Order removed.');
        $this->redirect(array('/shop/checkout'));
    }

    public function actionRevertChanges()
    {
        $BoxoCart = new BoxoCart;
        $BoxoCart->revertChanges();
        Yii::app()->user->setFlash('success', 'Changes reverted.');
        $this->redirect(array('/shop/checkout'));
    }

    public function actionCheckout()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('/shop/register'));
        }

        $userId = Yii::app()->user->id;
        $User = BoxomaticUser::model()->findByPk($userId);
        if (!$User->Location)
        {
            Yii::app()->user->setFlash('warning', 'Please set your location');
            $this->redirect(array('/user/update', 'id' => $User->id));
        }

        $DeliveryDates = DeliveryDate::model()->with('Boxes')->findAll(array(
            'condition' => 'DATE_SUB(date, INTERVAL -1 week) > NOW() AND date < DATE_ADD(NOW(), INTERVAL 1 MONTH)',
                //'limit'=>$show
        ));

        $BoxoCart = new BoxoCart;
        if (isset($_GET['setDate']))
        {
            $BoxoCart->setDelivery_date_id($_GET['setDate']);
        }

        $AllDeliveryDates = DeliveryDate::model()->with('Locations')->findAll('Locations.location_id = :locationId', array(
            ':locationId' => $User->location_id,
        ));

        if (isset($_POST['btn_recurring']))
        { //recurring order button pressed
            $NextDD = $BoxoCart->getLastDeliveryDate();
            $DDs = $BoxoCart->Location->getFutureDeliveryDates($NextDD, (int) $_POST['months_advance'], $_POST['every']);
            $allOk = $BoxoCart->repeatCurrentOrder($DDs);
            if (!$allOk)
            {
                Yii::app()->user->setFlash('warning', '<strong>Warning:</strong> One or more of the products are not available on the given dates and have been removed.');
            }
        }
        $this->render('checkout', array(
            'BoxoCart'         => $BoxoCart,
            'DeliveryDate'     => $BoxoCart->DeliveryDate,
            'Customer'         => $User,
            'AllDeliveryDates' => $AllDeliveryDates,
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error)
        {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm']))
        {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate())
            {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(SnapUtil::config('boxomatic/adminEmail'), $subject, $model->body, $headers);
                Yii::app()->user->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function actionRegister()
    {
        if (!Yii::app()->user->isGuest)
        {
            return $this->redirect(Yii::app()->homeUrl);
        }

        $BoxoCart = new BoxoCart;
        $Customer = new BoxomaticUser;

        $vars = array();
        if (isset($_POST['BoxomaticUser']))
        {
            $Customer->attributes = $_POST['BoxomaticUser'];
            $Customer->scenario = 'register';

            if ($Customer->save())
            {
                if (!$Customer->Location->is_pickup)
                {
                    $UserLoc = new UserLocation;
                    $UserLoc->user_id = $Customer->id;
                    $UserLoc->location_id = $Customer->location_id;
                    $UserLoc->address = $Customer->user_address;
                    $UserLoc->address2 = $Customer->user_address2;
                    $UserLoc->suburb = $Customer->user_suburb;
                    $UserLoc->state = $Customer->user_state;
                    $UserLoc->postcode = $Customer->user_postcode;
                    $UserLoc->phone = !empty($Customer->user_phone) ? $Customer->user_phone : $Customer->user_mobile;
                    $UserLoc->save(false);
                    $Customer->user_location_id = $UserLoc->customer_location_id;
                }

                $Auth = Yii::app()->authManager;
                $Auth->assign('customer', $Customer->id);

                $adminEmail = SnapUtil::config('boxomatic/adminEmail');
                $adminEmailFromName = SnapUtil::config('boxomatic/adminEmailFromName');
                //Send email
                $message = new YiiMailMessage('Welcome to ' . Yii::app()->name);
                $message->view = 'welcome';
                $message->setBody(array('User' => $Customer, 'newPassword' => $_POST['BoxomaticUser']['password']), 'text/html');
                $message->addTo($adminEmail);
                $message->addTo($Customer->email);
                $message->setFrom(array($adminEmail => $adminEmailFromName));

                if (!@Yii::app()->mail->send($message))
                {
                    $mailError = true;
                }

                $identity = new UserIdentity($Customer->email, $_POST['BoxomaticUser']['password']);
                $identity->authenticate();

                Yii::app()->user->login($identity);
                BoxomaticUser::model()->updateByPk($identity->id, array('last_login_time' => new CDbExpression('NOW()')));

                if ($BoxoCart->userBoxes || $BoxoCart->products)
                    $this->redirect(array('shop/checkout'));
                else
                    $this->redirect(Yii::app()->homeUrl);
            }
        }
        else
        {
            $Customer->location_id = $BoxoCart->location_id;
        }

        $Customer->password = '';
        $Customer->password_repeat = '';
        $vars['model'] = $Customer;

// $this->render('register',array('model'=>$Customer));
        $this->render('register', $vars);
    }

    /**
     * Welcome message.
     */
    public function actionWelcome()
    {
        $User = BoxomaticUser::model()->findByPk(Yii::app()->user->id);
        $this->render('welcome', array('User' => $User));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionPaypalFailure()
    {
        $this->render('paypalFailure');
    }

    public function actionPaypalSuccess()
    {
        Yii::import('frontend.modules.payPal.controllers.pdt.*');
        Yii::import('frontend.modules.payPal.components.*');
        $pdt = new PPPdtAction($this, "pdt");

        // Just invoking a success event, processing done by IPN listener
        $pdt->onRequest = function($event) {
            $event->sender->onSuccess($event);
        };

        // Notify user about successfull payment
        $pdt->onSuccess = function($event) {
            
            $transaction = UserPayment::model()->findByAttributes(array(
                'paypal_txn_id' => $event->details["txn_id"],
            ));
            if(!$transaction) {
                $transaction = new UserPayment;
            }

            // Put payment details into a transaction model
            $transaction->scenario = 'PaypalIPN';
            $transaction->paypal_payment_status = $event->details["payment_status"];
            $transaction->paypal_mc_currency = $event->details["mc_currency"];
            $transaction->paypal_mc_gross = $event->details["mc_gross"];
            $transaction->paypal_receiver_email = $event->details["receiver_email"];
            $transaction->paypal_txn_id = $event->details["txn_id"];
            $transaction->payment_value = $event->details["mc_gross"];
            $transaction->payment_type = 'PAYPAL';
            $transaction->payment_date = new CDbExpression('NOW()');
            $transaction->user_id = (int) $event->details["custom"];
            $transaction->payment_note = 'PayPal Payment of: '.$event->details["mc_gross"];
            Yii::app()->user->setFlash('success', 'Thanks! Your payment has been successfully processed.');
            
            $event->sender->controller->redirect(array('shop/checkout'));
        };

        // Notify user about failed payment
        $pdt->onFailure = function($event) {
            
            Yii::app()->user->setFlash('danger', 'Payment was not successful.');
            $event->sender->controller->redirect(array('shop/checkout'));
            //$event->sender->controller->renderText("Failure");
        };

        $pdt->run();
        
        //$this->render('paypalSuccess');
    }

    /**
     * @author Chienlv levanchien.it@gmail.com
     * @todo action process payment, support Paypal and Pin
     */
    public function actionProcess()
    {
        $BoxoCart = new BoxoCart;
        $BoxoCart->confirmOrder();
        $data = isset($_POST) ? $_POST : false;
        if ($data) {
            switch ($data['payment-method'])
            {
                /**
                 * @todo Default is Paypal
                 * @link http://stackoverflow.com/questions/14843212/submit-form-via-curl-and-redirect-browser-to-paypal tutoria
                 */
                default :
                case 0:
                    #Delete $ data ['payment-method'] because it is not for paypal
                    unset($data['payment-method']);
                    #The variable $data obtained from the form sent to
                    header('Location: https://www.paypal.com/cgi-bin/webscr?' . http_build_query($data));
                    break;
                /**
                 * @todo use Pin paymant
                 * Cac tham so gui tu from
                 * validate email? @toantv
                 */
                case 1:
                    $model = new PinPaymentForm();
                    if (isset($_POST['PinPaymentForm']))
                    {
                        $model->attributes = $_POST['PinPaymentForm'];
                        if ($model->validate())
                        {
                            $response = $model->pinPayMent();
                            if (isset($response['error']))
                            {
                                Yii::app()->user->setFlash('errors', $response);
                            }
                            elseif (isset($response['response']))
                            {
                                $model_pay = new UserPayment();
                                $model_pay->scenario = 'Pinpayment';
                                $model_pay->payment_date = new CDbExpression('NOW()');
                                $model_pay->payment_type = 'CREDIT-PIN';
                                $model_pay->payment_value = PinPaymentForm::float($response['response']['amount'] / 100);
                                $model_pay->user_id = Yii::app()->user->id;
                                $model_pay->staff_id = null;
                                $model_pay->pin_token = $response['response']['token'];
                                $model_pay->payment_note = 'Credit Card Payment';
                                $model_pay->save();

                                $BoxoCart = new BoxoCart;
                                $BoxoCart->confirmOrder();

                                Yii::app()->user->setFlash('success', "Thanks! Your payment has been successfully processed");

                                $this->redirect(array('shop/checkout'));
                            }
                            elseif ($response == null)
                            {
                                throw new CHttpException(404, 'Couldn\'t connect to Pin server. Please try again later!');
                            }
                        }
                    }

                    $this->render('process_pin', array(
                        'model'         => $model,
                        'paymentMethod' => $data['payment-method'],
                        'amount'        => $data['amount'],
                    ));
                    break;
                /**
                 * @todo Remove bank transfer
                 */
                /*
                  case 2:
                  //Send email
                  $adminEmail = SnapUtil::config('boxomatic/adminEmail');
                  $adminEmailFromName = SnapUtil::config('boxomatic/adminEmailFromName');
                  $message = new YiiMailMessage('Transfer information of ' . Yii::app()->name);
                  $message->view = 'bank_transfer';
                  $message->setBody($data, 'text/html');
                  $message->addTo($adminEmail);
                  $message->addTo($data['email']);
                  $message->setFrom(array($adminEmail => $adminEmailFromName));

                  Yii::app()->mail->send($message);

                  $model_pay = new UserPayment();
                  $model_pay->payment_date = new CDbExpression('NOW()');
                  $model_pay->payment_type = 'Offline-bank';
                  $model_pay->user_id = Yii::app()->user->id;
                  $model_pay->staff_id = null;
                  $model_pay->payment_note = 'Bank transfer';
                  $model_pay->save();

                  $BoxoCart = new BoxoCart;
                  $BoxoCart->confirmOrder();

                  $this->render('_bank_transfer', array('data' => $data));
                  break;
                 */
            }
        }
    }

}
