<?php
$cartEmpty = empty($BoxoCart->userBoxes) && empty($BoxoCart->products);
$controller = Yii::app()->controller;
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'extras-form',
    'enableAjaxValidation' => false,
));
?>
<div class="products order">
    <?php if(!$cartEmpty): ?>
    <div class="items list-view">
    <?php foreach($BoxoCart->userBoxes as $UserBox): ?>
        <?php echo $controller->renderPartial('//userBox/_view_cart',array(
            'data' => $UserBox,
        )) ?>
    <?php endforeach; ?>

    <?php foreach($BoxoCart->products as $SP): ?>
       <?php echo $controller->renderPartial('//orderItem/_view_cart',array(
           'data' => $SP,
           'form' => $form,
       )) ?>
    <?php endforeach; ?>
    </div>
    <hr />
    <div class="inner">
        <div class="row">
            <div class="col-xs-8">
                <strong>Total:</strong>
            </div>
            <div class="price-col col-xs-4">
                <strong><?php echo SnapFormat::currency($BoxoCart->total); ?></strong>
            </div>
        </div>
    </div>
    <?php echo CHtml::submitButton('Update Cart', array('class' => 'btn btn-sm btn-default pull-left', 'name' => 'update_cart')); ?>
    <?php echo !$cartEmpty ? CHtml::submitButton('Checkout', array('class'=>'btn btn-sm btn-success pull-right', 'name' => 'checkout')) : '' ?>

    <?php else: ?>
    <p>Nothing in cart.</p>
    <?php endif; ?>

</div>
<?php $this->endWidget(); ?>