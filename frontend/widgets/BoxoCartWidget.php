<?php

class BoxoCartWidget extends CWidget
{
    protected $_BoxoCart;

    public function init()
    {
        $BoxoCart = new BoxoCart;
        $controller = Yii::app()->controller;
        if (isset($_GET['setDate'])) {
            $BoxoCart->setDelivery_date_id($_GET['setDate']);
        }
        
        if (isset($_POST['BoxoCart']['location_id']))
        {
            $BoxoCart->setLocation_id($_POST['BoxoCart']['location_id']);
            Yii::app()->session['first_visit_page'] = 3;
            $controller->refresh();
        }
        
        $userId = Yii::app()->user->id;
        $User = BoxomaticUser::model()->findByPk($userId);

        //set location
        if ($User && $User->Location) {
            $BoxoCart->setLocation_id($User->location_id);
        }
        
        if (isset($_POST['Order']) && isset($_POST['add_to_cart']))
        {
            if ($BoxoCart->addItems($_POST['Order']))
            {
                foreach ($_POST["Order"] as $modelName => $orders)
                {
                    foreach ($orders as $id => $qty)
                    {
                        if ($modelName == "SupplierProduct")
                        {
                            $addedProduct = SupplierProduct::model()->findByPk($id);
                            Yii::app()->user->setFlash('success', $addedProduct->name . ' added to cart.');
                        }
                    }
                }

                $controller->redirect(array('shop/index', 'date' => Yii::app()->request->getParam('date'), 'cat' => $cat));
            }
            $controller->refresh();
        }

        if (isset($_POST['Order']) && (isset($_POST['update_cart']) || isset($_POST['checkout'])))
        {
            if ($BoxoCart->updateItems($_POST['Order'])) {
                Yii::app()->user->setFlash('success', 'Cart updated.');
            }
            if (isset($_POST['checkout'])) {
                $controller->redirect(array('shop/checkout'));
            }
            $controller->refresh();
        }

        $this->_BoxoCart = $BoxoCart;
    }

    public function run()
    {
        $this->render('boxocart', array(
            'BoxoCart' => $this->_BoxoCart,
        ));
    }

}
